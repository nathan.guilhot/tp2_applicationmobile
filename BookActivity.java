package com.example.uapv1705388.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class BookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        Intent intent = getIntent();
        final Bundle bundle = intent.getBundleExtra("book");

        final Book book = new Book(
                bundle.getLong("id"),
                bundle.getString("title"),
                bundle.getString("authors"),
                bundle.getString("year"),
                bundle.getString("genres"),
                bundle.getString("publisher")
                );

        final TextView viewtitle = (TextView) findViewById(R.id.nameBook);
        viewtitle.setText(book.getTitle());

        final TextView viewauthors = (TextView) findViewById(R.id.editAuthors);
        viewauthors.setText(book.getAuthors());

        final EditText viewyear = (EditText) findViewById(R.id.editYear);
        viewyear.setText(book.getYear());

        final EditText viewgenres = (EditText) findViewById(R.id.editGenres);
        viewgenres.setText(book.getGenres());

        final EditText viewpublisher = (EditText) findViewById(R.id.editPublisher);
        viewpublisher.setText(book.getPublisher());

        Button viewsavebutton = (Button) findViewById(R.id.button);

        viewsavebutton.setOnClickListener(new AdapterView.OnClickListener()
        {
            public void onClick(View v)
            {
                BookDbHelper bdh = new BookDbHelper(getApplicationContext());
                book.setTitle(viewtitle.getText().toString());
                book.setAuthors(viewauthors.getText().toString());
                book.setYear(viewyear.getText().toString());
                book.setGenres(viewgenres.getText().toString());
                book.setPublisher(viewpublisher.getText().toString());
                if(bdh.updateBook(book) <= 0)
                {
                    bdh.addBook(book);
                }
                Intent intent = new Intent();
                setResult(BookActivity.RESULT_OK, intent);
                finish();
            }
        });
    }
}

