package com.example.uapv1705388.tp2;

import android.app.Application;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listview = (ListView) findViewById(R.id.BookItem);

        final BookDbHelper bdh = new BookDbHelper(getApplicationContext());
        final Cursor allbookcursor = bdh.fetchAllBooks();

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                allbookcursor,
                new String[] { BookDbHelper.COLUMN_BOOK_TITLE,BookDbHelper.COLUMN_AUTHORS },
                new int[] { android.R.id.text1, android.R.id.text2},
                0);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {
                allbookcursor.move(0);
                Book book = BookDbHelper.cursorToBook(allbookcursor);
                Intent intent =  new Intent(MainActivity.this,BookActivity.class);
                Bundle bundle = new Bundle();
                bundle.putLong("id",book.getId());
                bundle.putString("title",book.getTitle());
                bundle.putString("authors",book.getAuthors());
                bundle.putString("year",book.getYear());
                bundle.putString("genres",book.getGenres());
                bundle.putString("publisher",book.getPublisher());
                intent.putExtra("book",bundle);
                startActivityForResult(intent, 1);
            }
        });

        listview.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener(){
            @Override
            public void onCreateContextMenu(ContextMenu p1, View p2, ContextMenu.ContextMenuInfo p3)
            {
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.bookmenu, p1);
            }
        });

        FloatingActionButton viewaddbutton = (FloatingActionButton) findViewById(R.id.AddButton);

        viewaddbutton.setOnClickListener(new AdapterView.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent intent =  new Intent(MainActivity.this,BookActivity.class);
                Bundle bundle = new Bundle();
                bundle.putLong("id",0);
                bundle.putString("title","");
                bundle.putString("authors","");
                bundle.putString("year","");
                bundle.putString("genres","");
                bundle.putString("publisher","");
                intent.putExtra("book",bundle);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1)
        {
            if (resultCode == BookActivity.RESULT_OK) {
                this.recreate();
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.BookMenuDelete:
                final BookDbHelper bdh = new BookDbHelper(getApplicationContext());
                final Cursor allbookcursor = bdh.fetchAllBooks();
                allbookcursor.move(info.position);
                bdh.deleteBook(allbookcursor);
                recreate();
                return true;
            default:
                return MainActivity.super.onContextItemSelected(item);
        }
    }



}
